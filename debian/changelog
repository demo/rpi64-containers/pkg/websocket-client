websocket-client (0.57.0-1+apertis1) apertis; urgency=medium

  * Refresh the automatically detected licensing information.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Thu, 26 Aug 2021 11:06:38 -0300

websocket-client (0.57.0-1+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 26 Aug 2021 09:34:16 -0300

websocket-client (0.57.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sophie Brun ]
  * New upstream version 0.57.0 (Closes: #960535)
  * Upstream license changes to BSD-3-clause
  * Update debian/copyright: remove useless entry
  * Add missing description of patch

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 27 Jul 2020 14:29:29 +0200

websocket-client (0.53.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938814
  * Bump Standards-Version to 4.4.1 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sat, 02 Nov 2019 16:56:44 -0400

websocket-client (0.53.0-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Christoph Berg ]
  * New upstream version.
  * Remove python-backports.ssl-match-hostname dependency. (Closes: #909973)

 -- Christoph Berg <christoph.berg@credativ.de>  Mon, 01 Oct 2018 10:54:13 +0200

websocket-client (0.48.0-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Alexander GQ Gerasiov ]
  * New upstream release.
  * 0001-Use-debian-global-ca-cert-store-instead-of-local-mozilla-copy removed:
    fixed upstream.
  * Remove postrm scripts: update-alternative --remove is called from prerm.

 -- Alexander GQ Gerasiov <gq@debian.org>  Tue, 10 Jul 2018 17:24:47 +0300

websocket-client (0.37.0-2) unstable; urgency=medium

  * Add patch:
    - 0001-Use-debian-global-ca-cert-store-instead-of-local-mozilla-copy

 -- Thomas Goirand <zigo@debian.org>  Thu, 08 Sep 2016 15:45:45 +0000

websocket-client (0.37.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Thomas Goirand ]
  * New upstream release (Closes: #810088)
  * Removed now useless version in python-all build-depends.
  * Removed useless X-Python{3,}-Version headers.
  * Standards-Version is now 3.9.8 (no change).
  * Fixed duplicate short desc.
  * Using update-alternatives to manage /usr/bin/wsdump.
  * Add myself as uploader.
  * Ran wrap-and-sort -t -a.

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Jul 2016 22:54:11 +0200

websocket-client (0.18.0-2) unstable; urgency=medium

  * Team upload.
  * Do not ship tests module in python3-websocket binary package either
    (Closes: #767400)
    - Thanks to Teemu Hukkanen for the patch

 -- Scott Kitterman <scott@kitterman.com>  Fri, 14 Nov 2014 00:50:57 -0500

websocket-client (0.18.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Barry Warsaw <barry@debian.org>  Thu, 25 Sep 2014 09:40:28 -0400

websocket-client (0.16.0a-2) unstable; urgency=medium

  * Team upload.
  * Add python3-websocket binary package. (Closes: #749943)
  * d/control: wrap-and-sort

 -- Barry Warsaw <barry@debian.org>  Wed, 20 Aug 2014 17:01:51 -0400

websocket-client (0.16.0a-1) unstable; urgency=medium

  * New upstream release
    - Include the test-suite and run it at build-time (Closes: #741606, #744726)
  * Switch buildsystem to pybuild
  * Update copyright file
  * Do not ship the tests module in the binary

 -- Nicolas Dandrimont <olasd@debian.org>  Fri, 15 Aug 2014 23:51:19 +0200

websocket-client (0.12.0-1) unstable; urgency=medium

  * Initial release. (Closes: #727759)

 -- Nicolas Dandrimont <olasd@debian.org>  Sat, 21 Dec 2013 21:32:31 +0100
